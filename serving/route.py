from flask import request
from flask_restx import Resource, Api, Namespace, fields
from util import *
from fields import *

column_code_to_value_dict = get_column_code_to_value()
ns = nsnamespace

@ns.route("column_code_to_value")
@ns.doc(params={"column_code": "found"})
class nsSimple(Resource):
    @ns.response(200, "Success", column_code_to_value_fields)
    @ns.response(500, "Failed")
    def get(self):
        """column의 code와 대응되는 value를 가져옵니다."""

        column_code = request.args.get("column_code")
        print(column_code)
        return {
            "value": column_code_to_value_dict[column_code]
        }

@ns.route("current_salary")
class current_salary(Resource):
    @ns.response(200, "Success")
    @ns.response(500, "Failed")
    def get(self):
        """학교 소재지별 현 직장(일자리) 월평균 근로소득_만원"""

        return get_current_salary()

@ns.route("salary_by_college")
class salary_by_college(Resource):
    @ns.response(200, "Success")
    @ns.response(500, "Failed")
    def get(self):
        """단과대별 현 직장(일자리) 월평균 근로소득_만원"""

        return get_salary_by_college()

@ns.route("salary_by_major")
class salary_by_major(Resource):
    @ns.response(200, "Success")
    @ns.response(500, "Failed")
    def get(self):
        """학과별 현 직장(일자리) 월평균 근로소득_만원"""

        return get_salary_by_major()

@ns.route("salary_by_age_and_college")
class salary_by_age_and_college(Resource):
    @ns.response(200, "Success")
    @ns.response(500, "Failed")
    def get(self):
        """학과별 현 직장(일자리) 월평균 근로소득_만원"""

        return get_salary_by_major()