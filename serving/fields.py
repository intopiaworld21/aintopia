from flask_restx import Namespace, fields

nsnamespace = Namespace(
    name="API",
    description="API document",
)

column_code_to_value_fields = nsnamespace.model("code_to_value", {
    "column_code": fields.String(description="code of column", required=True, example="found"),
})