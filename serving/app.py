from flask import Flask
from flask_restx import Resource, Api
from route import ns

app = Flask(__name__)
api = Api(
    app,
    version='0.1',
    title="Intopia API",
    description="Intopia API for other company",
    contact="admin@intopia.org"
)

api.add_namespace(ns, '/api/')

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=18000)